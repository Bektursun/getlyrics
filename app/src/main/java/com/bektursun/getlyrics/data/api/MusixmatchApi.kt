package com.bektursun.getlyrics.data.api

import com.bektursun.getlyrics.data.model.SearchTrack
import com.bektursun.getlyrics.utils.Constants
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MusixmatchApi {

    @GET("track.search")
    fun searchLyrics(
        @Query("q_track")
        trackName: String,
        @Query("page_size")
        pageSize: Int = 5,
        @Query("apikey")
        apiKey: String = Constants.API_KEY
    ): Observable<SearchTrack>

}