package com.bektursun.getlyrics.data.repository

import com.bektursun.getlyrics.data.api.AppRemoteSource
import com.bektursun.getlyrics.data.model.SearchTrack
import io.reactivex.Observable

data class Result<out T>(
    val data: T? = null,
    val error: Throwable? = null
)

fun <T> T.asResult(): Result<T> =  Result(data = this, error = null)

open class MusixmatchRepository (private val api: AppRemoteSource) : Repository {

    override fun searchTrack(trackName: String): Observable<Result<SearchTrack>> =
        api.searchTrack(trackName)
            .flatMap {
                Observable.just(it.asResult())
            }
            .onErrorResumeNext { t: Throwable ->
                return@onErrorResumeNext Observable.just(
                    Result(null, t)
                )
            }

}
