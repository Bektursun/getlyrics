package com.bektursun.getlyrics.data.model


import com.google.gson.annotations.SerializedName

data class PrimaryGenres(
    @SerializedName("music_genre_list")
    val musicGenreList: List<MusicGenre>
)