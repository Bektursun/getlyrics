package com.bektursun.getlyrics.data.api

class AppRemoteSource(private val api: MusixmatchApi) {

    fun searchTrack(trackName: String) = api.searchLyrics(trackName)
}