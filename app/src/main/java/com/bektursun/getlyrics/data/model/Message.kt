package com.bektursun.getlyrics.data.model


import com.google.gson.annotations.SerializedName

data class Message(
    @SerializedName("body")
    val body: Body,
    @SerializedName("header")
    val header: Header
)