package com.bektursun.getlyrics.data.model


import com.google.gson.annotations.SerializedName

data class Track(
    @SerializedName("track")
    val track: TrackX
)