package com.bektursun.getlyrics.data.model


import com.google.gson.annotations.SerializedName

data class SearchTrack(
    @SerializedName("message")
    val message: Message
)