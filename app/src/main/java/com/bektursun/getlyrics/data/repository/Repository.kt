package com.bektursun.getlyrics.data.repository

import com.bektursun.getlyrics.data.model.SearchTrack
import io.reactivex.Observable

interface Repository {

    fun searchTrack(trackName: String): Observable<Result<SearchTrack>>
}