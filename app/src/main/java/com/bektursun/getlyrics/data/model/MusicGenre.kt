package com.bektursun.getlyrics.data.model


import com.google.gson.annotations.SerializedName

data class MusicGenre(
    @SerializedName("music_genre")
    val musicGenre: MusicGenreX
)