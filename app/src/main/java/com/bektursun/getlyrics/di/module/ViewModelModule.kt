package com.bektursun.getlyrics.di.module

import com.bektursun.getlyrics.ui.fragment.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(get()) }
}