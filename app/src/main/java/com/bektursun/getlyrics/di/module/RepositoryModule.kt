package com.bektursun.getlyrics.di.module

import com.bektursun.getlyrics.data.repository.MusixmatchRepository
import com.bektursun.getlyrics.data.repository.Repository
import org.koin.dsl.module

val repositoryModule = module {
    factory<Repository> {
        MusixmatchRepository(get())
    }
}