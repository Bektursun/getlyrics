package com.bektursun.getlyrics.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bektursun.getlyrics.R
import com.bektursun.getlyrics.base.BaseFragment
import com.bektursun.getlyrics.data.model.SearchTrack
import com.bektursun.getlyrics.databinding.MainFragmentBinding
import com.bektursun.getlyrics.utils.observe
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MainFragment : BaseFragment() {

    private lateinit var binding: MainFragmentBinding
    private val viewModel by viewModel<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false)
        viewModel.searchTrack("Humble")
        return binding.root
    }

    override fun observeChange() {
        observe(viewModel.searchTrack, ::getTrack)
    }

    private fun getTrack(data: SearchTrack) {
        Timber.d("${data.message}")
    }
}