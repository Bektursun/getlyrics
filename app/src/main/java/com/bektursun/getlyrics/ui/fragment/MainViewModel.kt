package com.bektursun.getlyrics.ui.fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bektursun.getlyrics.base.BaseViewModel
import com.bektursun.getlyrics.data.model.SearchTrack
import com.bektursun.getlyrics.data.repository.Repository
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class MainViewModel(private val repository: Repository) : BaseViewModel() {


    private val _searchTrack: MutableLiveData<SearchTrack> = MutableLiveData()
    val searchTrack: LiveData<SearchTrack>
        get() = _searchTrack


    fun searchTrack(trackName: String) {
        val currentTrack = repository.searchTrack(trackName)
            .observeOn(Schedulers.io())
            .doOnSubscribe { Timber.i("doOnSubscribe()") }
            .doOnTerminate { Timber.i("doOnTerminate") }
            .subscribe({ result -> _searchTrack.postValue(result.data) },
                { e -> Timber.e("$e") })
        compositeDisposable.add(currentTrack)
    }

}