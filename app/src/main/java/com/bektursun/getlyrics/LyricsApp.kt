package com.bektursun.getlyrics

import android.app.Application
import com.bektursun.getlyrics.di.module.networkModule
import com.bektursun.getlyrics.di.module.repositoryModule
import com.bektursun.getlyrics.di.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class LyricsApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@LyricsApp)
            modules(networkModule)
            modules(repositoryModule)
            modules(viewModelModule)
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}